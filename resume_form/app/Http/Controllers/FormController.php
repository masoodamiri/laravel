<?php


namespace App\Http\Controllers;


use App\Http\Requests\InputForm;
use App\Myuser;
use App\Skill;
use Symfony\Component\HttpFoundation\Session\Session;

class FormController
{
    private $id;

    /**
     * show form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('user_form');
    }

    /**
     * get user info and send to db
     *
     * @param InputForm $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(InputForm $request)
    {
        $attributes = $request->except("skills", "resume_file");
        $attributes['resume_file'] = $request->file("resume_file")->store("", "resume"); //storing file and getting its name
        $user = Myuser::create($attributes); //insert in users table
        $skills = Skill::find($request->input('skills')); //retrieving skills from table
        $user->skills()->attach($skills); //insert into pivot
        session(['user_id' => $user->id]); //set user table id in session
        return redirect(route('resume_view'));
    }
}
