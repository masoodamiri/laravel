<?php


namespace App\Http\Controllers;


use App\Myuser;
use App\Skill;
use Illuminate\Support\Facades\Storage;
use function mysql_xdevapi\getSession;

class ResumeController
{
    /**
     * show resume
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResume()
    {
        $user_id = session('user_id'); //get session for user table id
        $resume = Myuser::find($user_id); //get user record from users table and pivot
        return view("resume", compact("resume")); //sending records to view
    }

    /**
     * download resume file
     *
     * @param $file
     * @return mixed
     */
    public function download($file)
    {
        return Storage::disk('resume')->download($file);
    }

}
