<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InputForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|alpha|min:2|max:20',
            'last_name' => 'required|alpha|min:2|max:20',
            'email' => 'nullable|email',
            'phone' => 'nullable|numeric',
            'resume_file' => 'required|file|mimes:pdf|max:5120',
            'skills' => 'required|array|exists:skills,id|min:3'
        ];
    }
}
