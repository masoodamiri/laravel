<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Myuser extends Model
{
    protected $fillable = ["first_name", "last_name", "email", "phone", "resume_file"];

    public function skills()
    {
        return $this->belongsToMany(Skill::class);
    }

}
