<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyuserSkillTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'myuser_skill';

    /**
     * Run the migrations.
     * @table myusers_skills
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('skill_id');
            $table->unsignedInteger('myuser_id');

            $table->index(["myuser_id", "id"], 'myuser_id_idx');

            $table->index(["skill_id", "id"], 'skill_id_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('skill_id', 'skill_id_idx')
                ->references('id')->on('skills')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('myuser_id', 'myuser_id_idx')
                ->references('id')->on('myusers')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
