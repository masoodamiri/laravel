<?php
return
    [
        'name' => 'Full Name',
        'email' => 'Email Address',
        'phone' => 'Phone',
        'submit' => 'Submit',
    ];
