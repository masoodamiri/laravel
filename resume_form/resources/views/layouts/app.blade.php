<!doctype html>
<html>
<head>
    <title>@yield('title')</title>
    @section('stylesheets')
        <link rel="stylesheet" href={{asset('css/bootstrap.css')}}>
    @show
</head>
<body class="container">
<div class="row">
    <div class="col-xl-12">
        @section('head')
            <img style="width:100%;height: 200px;margin-bottom: 50px" src="{{'images/img.jpg'}}">
        @show
    </div>
</div>
<h1 class="h1 text-center" style="margin-bottom: 50px">@yield('header')</h1>
<div class="row justify-content-center">
    <div class="col-xl-8">
        @yield('content')
    </div>
</div>
<div style="margin-top: 50px" class="card-footer row justify-content-sm-center ">
    <div class="col-sm-2 text-center">Masood Amiri</div>
</div>

@section('javascripts')
    <script src={{asset('js/bootstrap.bundle.js')}}></script>
    <script src={{asset("js/jquery-3.4.1.js")}}></script>
    <script src={{asset("js/event.js")}}></script>
@show
</body>
</html>
