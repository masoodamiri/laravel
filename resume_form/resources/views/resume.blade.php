@extends('layouts.app')
@section('title','Resume')
@section('stylesheets')
    @parent
    <!--another stylesheet for this page-->
@endsection
@section('header','Resume')
@section('content')
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col">Skills</th>
            <th scope="col">Resume</th>
        </tr>
        </thead>
        <tbody>
            <tr class="d-table-row">
                <td class="d-table-cell">{{$resume->first_name}} {{$resume->last_name}}</td>
                <td class="d-table-cell">{{$resume->email}}</td>
                <td class="d-table-cell">{{$resume->phone}}</td>
                <td class="d-table-cell">@foreach($resume->skills as $skill)
                        {{$skill->name.','}}
                    @endforeach
                </td>
                <td class="d-table-cell"><a href="{{route("resume_download",['resume_file'=>$resume->resume_file])}}">Download</a></td>
            </tr>
        </tbody>
    </table>

@endsection
