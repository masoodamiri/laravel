@extends('layouts.app')
@section('title','Form')
@section('stylesheets')
@parent
<link rel="stylesheet" href="{{asset('css/form.css')}}">
@endsection
@section('header','Form')
@section('content')
<form action="{{route('form_store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group row">
        <label class="col-sm-2">Full Name</label>
        <div class="col-sm-5">
            <input type="text" name="first_name" class="form-control" placeholder="First Name">
            @error('first_name')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>
        <div class="col-sm-5">
            <input type="text" name="last_name" class="form-control" placeholder="Last Name">
            @error('last_name')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
            <input type="email" name="email" class="form-control" placeholder="Email">
            @error('email')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Phone</label>
        <div class="col-sm-10">
            <input type="text" name="phone" class="form-control" placeholder="Phone">
            @error('phone')
            <div class="alert alert-danger">{{$message}}</div>
            @enderror
        </div>
    </div>
    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-2 pt-0">Skills</legend>
            <div class="col-sm-10">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="skills[]" value="1">
                    <label class="form-check-label">
                        PHP
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="skills[]" value="2">
                    <label class="form-check-label">
                        Laravel
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="skills[]" value="3">
                    <label class="form-check-label">
                        MySql
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="skills[]" value="4">
                    <label class="form-check-label">
                        JavaScript
                    </label>
                </div>
                @error('skills')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
        </div>
    </fieldset>
    <div class="form-group row">
        <div class="col-sm-2">Resume File</div>
        <div class="col-sm-10">
            <div>
                <input type="file" name="resume_file" class="form-control-file" id="exampleFormControlFile1">
                @error('resume_file')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
        </div>
    </div>
    <div class="form-group row justify-content-sm-center">
        <div class="col-sm-2">
            <button type="submit" class="btn-lg btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection
