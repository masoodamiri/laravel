<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FormController@index')->name('form_view');
Route::post('store', 'FormController@store')->name('form_store');
Route::get('resume', 'ResumeController@showResume')->name('resume_view');
Route::get("/resume/{resume_file}", 'ResumeController@download')->name("resume_download");
